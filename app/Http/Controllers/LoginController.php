<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function autentikasi(Request $request)
    {
        Session::flash('email', $request->email);
        Session::flash('password', $request->password);

        $validasi = $request->validate(
            [
                'email' => 'required|email',
                'password' => 'required'
            ],
            [
                'email.required' => 'Email wajib diisi',
                'email.email' => 'Email tidak valid',
                'password.required' => 'Password wajib diisi'
            ]
        );

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // jika autentikasi berhasil
            $request->session()->regenerate();
            session()->flash('success', 'Login berhasil');

            if (auth()->user()->level == 'admin') {
                return redirect('/dashboard-admin');
            } elseif (auth()->user()->level == 'user') {
                return redirect()->intended('/user');
            } else {
                return redirect()->intended('/');
            }
        } else {
            // jika autentikasi gagal
            return redirect('/')->with('notvalid', 'Email dan password yang dimasukkan tidak valid');
        }
    }

    public function admin()
    {
        return view('admin.dashboard');
    }
}
