<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pegawai = Pegawai::all();
        return view('admin.daftar-pegawai', compact('pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.createpegawai');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required',
            'telepon' => 'required',
            'alamat' => 'required',
            'jabatan' => 'required',
        ],[
            'nama' => 'Nama Wajib Diisi',
            'telepon' => 'No Telepon Wajib Diisi',
            'alamat' => 'Alamat Wajib Diisi',
            'jabatan' => 'Jabatan Wajib Diisi',
        ]);


        $pegawai = new Pegawai;
        $pegawai->nama = $request->nama;
        $pegawai->telepon = $request->telepon;
        $pegawai->alamat = $request->alamat;
        $pegawai->jabatan = $request->jabatan;
        $pegawai->save();

        return redirect()->route('data-pegawai')->with('success', 'Data berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pegawai = Pegawai::findOrFail($id);
        return view('admin.editpegawai', compact('pegawai'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        $data = $request->validate([
            'nama' => 'required',
            'telepon' => 'required',
            'alamat' => 'required',
            'jabatan' => 'required',
        ],[
            'nama' => 'Nama Wajib Diisi',
            'telepon' => 'No Telepon Wajib Diisi',
            'alamat' => 'Alamat Wajib Diisi',
            'jabatan' => 'Jabatan Wajib Diisi',
        ]);

        $pegawai = Pegawai::findOrFail($id);

        $pegawai->nama = $request->nama;
        $pegawai->telepon = $request->telepon;
        $pegawai->alamat = $request->alamat;
        $pegawai->jabatan = $request->jabatan;
        $pegawai->save();

        return redirect()->route('data-pegawai')->with('success', 'Data berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $pegawai = Pegawai::findOrFail($id);
        $pegawai->delete();

        return redirect()->route('data-pegawai')->with('success', 'Data berhasil dihapus.');
    }
}
