<div class="sticky top-0 bg-[#27ae60] border-b-2 border-black/50">
    <nav class="flex justify-between items-center px-20 py-3">
        <div class="flex items-center">
            <img src="{{ asset('images/logo.png') }}" alt="Logo" class="w-10">
            <button class="text-white font-bold text-2xl">DATA PEGAWAI</button>
        </div>
        <div class="flex items-center">
            <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="px-4 py-1 text-white hover:underline rounded-full ml-10">Logout</button>
            </form>
        </div>
    </nav>
</div>
