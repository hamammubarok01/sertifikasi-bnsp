@extends('admin.admin_layouts.master')

@section('title', 'admin/tambah-buku')

@section('content')

    <div class="px-4 py-10 min-h-screen md:px-20">
        <div class="p-5 mx-auto w-full max-w-3xl shadow-lg border-2 border-black/10 rounded-2xl">
            <div>
                <h1 class="text-2xl text-center font-bold uppercase">Tambah Data Pegawai</h1>
            </div>
            <div>
                <form action="{{ route('store-pegawai') }}" method="POST">
                    @csrf
                    <div class="mt-5 grid grid-cols-1 md:grid-cols-2 gap-x-8 gap-y-4">
                        <div>
                            <p class="font-medium">Nama</p>
                            <input type="text" name="nama" placeholder="masukkan nama pegawai" class="focus:outline-0 mt-2 border-2 border-[#27ae60]/50 h-11 w-full pl-3 rounded-lg">
                            {{-- pesan error  --}}
                            @error('nama')
                                <p class="text-red-800">{{ $message }}</p>
                            @enderror
                        </div>
                        <div>
                            <p class="font-medium">Telepon</p>
                            <input type="text" name="telepon" placeholder="masukkan no telepon" min="1" class="focus:outline-0 mt-2 border-2 border-[#27ae60]/50 h-11 w-full pl-3 rounded-lg">
                            {{-- pesan error  --}}
                            @error('telepon')
                                <p class="text-red-800">{{ $message }}</p>
                            @enderror
                        </div>

                        <div>
                            <p class="font-medium">Alamat</p>
                            <input type="text" name="alamat" placeholder="masukkan alamat" class="focus:outline-0 mt-2 border-2 border-[#27ae60]/50 h-11 w-full pl-3 rounded-lg">
                            {{-- pesan error  --}}
                            @error('alamat')
                                <p class="text-red-800">{{ $message }}</p>
                            @enderror
                        </div>
                        <div>
                            <p class="font-medium">Jabatan</p>
                            <input type="text" name="jabatan" placeholder="masukkan jabatan" class="focus:outline-0 mt-2 border-2 border-[#27ae60]/50 h-11 w-full pl-3 rounded-lg">
                            {{-- pesan error  --}}
                            @error('jabatan')
                                <p class="text-red-800">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="mt-8 h-11 w-full rounded-lg bg-[#27ae60] font-medium text-white flex justify-center items-center">
                        <button type="submit" name="submit">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
