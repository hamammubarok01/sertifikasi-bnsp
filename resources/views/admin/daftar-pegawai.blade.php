@extends('admin.admin_layouts.master')

@section('title', 'admin/daftar-pegawai')

@section('content')

<div class="px-4 py-10 min-h-screen md:px-16 lg:px-32">
    <div class="flex flex-col md:flex-row min-h-[36rem] rounded-3xl border-2 border-[#27ae60]/50 shadow-lg overflow-hidden">
        <div class="w-full md:w-1/4 px-4 py-6 border-b-2 md:border-b-0 md:border-r-2 border-[#27ae60]/50 rounded-3xl">
            <div class="flex items-center bg-[#219150] px-4 py-2 text-white rounded">
                <div>
                    <svg class="w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                    </svg>
                </div>
                <div>
                    <a href="{{ route('dashboard-admin') }}" class="navLink2 pl-2 font-bold text-xl">Dashboard</a>
                </div>
            </div>
            <div class="mt-2 flex items-center bg-[#219150] px-4 py-2 text-white rounded">
                <div>
                    <svg class="w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                    </svg>
                </div>
                <div>
                    <a href="{{ route('data-pegawai') }}" class="navLink2 pl-2 font-bold text-xl">Pegawai</a>
                </div>
            </div>
        </div>
        <div class="w-full md:w-3/4 px-4 py-6">
            <div class="flex flex-col md:flex-row md:justify-between">
                <h1 class="font-bold text-2xl">Daftar Pegawai</h1>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success mt-3 md:mt-0">
                        {{ $message }}
                    </div>
                @endif
                <a href="{{ route('create-pegawai') }}" class="mt-3 md:mt-0">
                    <button class="bg-[#219150] px-4 py-2 text-white rounded-3xl font-medium">Tambah</button>
                </a>
            </div>
            <div class="mt-5 grid grid-cols-1 md:grid-cols-5 gap-2">
                <p class="px-4 py-2 font-medium bg-[#27ae60]/50 rounded-tl-lg text-sm md:text-base lg:text-lg truncate">Nama</p>
                <p class="px-4 py-2 font-medium bg-[#27ae60]/50 text-sm md:text-base lg:text-lg truncate">Telepon</p>
                <p class="px-4 py-2 font-medium bg-[#27ae60]/50 text-sm md:text-base lg:text-lg truncate">Alamat</p>
                <p class="px-4 py-2 font-medium bg-[#27ae60]/50 text-sm md:text-base lg:text-lg truncate">Jabatan</p>
                <p class="px-4 py-2 font-medium bg-[#27ae60]/50 rounded-tr-lg text-sm md:text-base lg:text-lg truncate">Tindakan</p>
            </div>

            @foreach ($pegawai as $item)
                <div class="grid grid-cols-1 md:grid-cols-5 gap-2">
                    <p class="px-4 py-2 border-b-[1px] border-black/50 text-sm md:text-base lg:text-lg truncate">{{ $item->nama }}</p>
                    <p class="px-4 py-2 border-b-[1px] border-black/50 text-sm md:text-base lg:text-lg truncate">{{ $item->telepon }}</p>
                    <p class="px-4 py-2 border-b-[1px] border-black/50 text-sm md:text-base lg:text-lg truncate">{{ $item->alamat }}</p>
                    <p class="px-4 py-2 border-b-[1px] border-black/50 text-sm md:text-base lg:text-lg truncate">{{ $item->jabatan }}</p>
                    <div class="px-4 py-2 border-b-[1px] border-black/50 flex justify-center items-center space-x-4">
                        <button onclick="openModal('{{ $item->id }}')" class="bg-yellow-500 px-4 py-2 text-white rounded-3xl font-medium">
                            <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15 12c0 1.657-1.343 3-3 3S9 13.657 9 12s1.343-3 3-3 3 1.343 3 3z" />
                                <path stroke-linecap="round" stroke-linejoin="round" d="M2.458 12C3.732 7.943 7.522 5 12 5s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7s-8.268-2.943-9.542-7z" />
                            </svg>
                        </button>
                        <form action="{{ route('delete-pegawai', $item->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data?');">
                                <svg class="w-8 h-8 bg-white-800 shadow rounded-full p-1 text-red-800 border-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.02-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                </svg>
                            </button>
                        </form>

                        <a href="{{ route('edit-pegawai', $item->id) }}">
                            <button type="submit">
                                <svg class="w-8 h-8 bg-sky-800 shadow rounded-full p-1 text-white border-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                                </svg>
                            </button>
                        </a>
                    </div>
                </div>

                <!-- Modal -->
                <div id="modal-{{ $item->id }}" class="hidden fixed inset-0 bg-gray-600 bg-opacity-50 overflow-y-auto h-full w-full">
                    <div class="relative top-20 mx-auto p-5 border w-96 shadow-lg rounded-md bg-white">
                        <div class="mt-3 text-center">
                            <h3 class="text-lg leading-6 font-medium text-gray-900">{{ $item->nama }}</h3>
                            <div class="mt-2 px-7 py-3">
                                <p class="text-sm text-gray-500">
                                    Telepon: {{ $item->telepon }} <br>
                                    Alamat: {{ $item->alamat }} <br>
                                    Jabatan: {{ $item->jabatan }} <br>
                                    <!-- Add more details if necessary -->
                                </p>
                            </div>
                            <div class="items-center px-4 py-3">
                                <button class="bg-blue-500 px-4 py-2 text-white text-base font-medium rounded-md w-full shadow-sm hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-blue-300" onclick="closeModal('{{ $item->id }}')">
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

<script>
    function openModal(id) {
        document.getElementById('modal-' + id).classList.remove('hidden');
    }

    function closeModal(id) {
        document.getElementById('modal-' + id).classList.add('hidden');
    }
</script>

@endsection
