<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// register
Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/daftar', [RegisterController::class, 'daftar'])->name('daftar');

// login
Route::get('/', [LoginController::class, 'index'])->name('login');
Route::post('/autentikasi', [LoginController::class, 'autentikasi'])->name('autentikasi');
Route::get('/dashboard-admin', [LoginController::class, 'admin'])->name('dashboard-admin');

// logout
Route::post('/logout', [LogoutController::class, 'index'])->name('logout');

// data pegawai
Route::get('/pegawai', [PegawaiController::class, 'index'])->name('data-pegawai');
Route::get('/create', [PegawaiController::class, 'create'])->name('create-pegawai');
Route::post('/store', [PegawaiController::class, 'store'])->name('store-pegawai');
Route::get('/pegawai/edit/{id}', [PegawaiController::class, 'edit'])->name('edit-pegawai');
Route::put('/pegawai/{id}', [PegawaiController::class, 'update'])->name('update-pegawai');
Route::delete('/pegawai/{id}', [PegawaiController::class, 'destroy'])->name('delete-pegawai');





